package br.com.itau;

public class Cliente {

    private String nome;
    private String cpf;
    private int idade;

    public Cliente(String nome, String cpf, int idade) {
        if (idade < 18) {
            throw new ArithmeticException("Infelizmente não aceitamos menores de 18 anos.");
        }
        else {
            this.nome = nome;
            this.cpf = cpf;
            this.idade = idade;
        }

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    @Override
    public String toString(){
        String string;
        string = new String("cpf:" + this.cpf
                + ", nome:" + this.nome + ", idade:" + this.idade);
        return string;
    }

}
