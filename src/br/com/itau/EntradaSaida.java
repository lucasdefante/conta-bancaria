package br.com.itau;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class EntradaSaida {

    public static Map<String, String> getDados() {
        Scanner teclado = new Scanner(System.in);
        Map<String, String> dados = new HashMap<>();

        System.out.println("Qual sua idade?");
        dados.put("idade", teclado.nextLine());

        System.out.println("Qual seu nome?");
        dados.put("nome", teclado.nextLine());

        System.out.println("Qual seu CPF?");
        dados.put("cpf", teclado.nextLine());

        return dados;

    }

    public static double getValorDeposito() {
        Scanner teclado = new Scanner(System.in);
        System.out.println("Digite o valor a ser depositado:");
        return teclado.nextDouble();
    }

    public static double getValorSaque() {
        Scanner teclado = new Scanner(System.in);
        System.out.println("Digite o valor do saque:");
        return teclado.nextDouble();
    }

}
