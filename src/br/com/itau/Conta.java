package br.com.itau;


import java.text.DecimalFormat;

public class Conta {

    private Cliente client;
    private double saldo;
    DecimalFormat df = new DecimalFormat("###,###,###.00");

    public Conta(Cliente client) {
        this.client = client;
        this.saldo = 0.00;
    }

    public void sacar(double saque) {
        this.saldo -= saque;
    }

    public void depositar(double deposito) {
        this.saldo += deposito;
    }

    public Cliente getClient() {
        return this.client;
    }

    public void setClient(Cliente client) {
        this.client = client;
    }

    public String getSaldo() {
        return df.format(this.saldo);
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

}
