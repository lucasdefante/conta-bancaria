package br.com.itau;

import java.util.Map;

public class Main {

    public static void main(String[] args) {
        Map<String, String> pessoa = EntradaSaida.getDados();

        try {
            Cliente client1 = new Cliente(
                    pessoa.get("nome"),
                    pessoa.get("cpf"),
                    Integer.parseInt(pessoa.get("idade"))
            );
            Conta cont1 = new Conta(client1);
            System.out.println("Conta criada com sucesso.");

            // Primeira ação da conta é a realização de um depósito
            cont1.depositar(EntradaSaida.getValorDeposito());

            // Segunda ação é a realização de um saque (não há validação de saldo, o cliente pode ficar negativado)
            cont1.sacar(EntradaSaida.getValorSaque());

            // Terceira ação é a consulta do saldo da conta
            System.out.println("Saldo da conta: R$ " + cont1.getSaldo());

            // Quarta ação é a consulta do cliente vinculado a conta
            System.out.println("Cliente: " + cont1.getClient().toString());
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
